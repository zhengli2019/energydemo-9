# Clean Engergy Demo
Collection of scripts to extract projections from CORDEX simulation outputs that may be of interest to Clean Energy Sector applications. 
Focus is on flexible routines for working with files as downloaded from the public data bases and on potential visulisation approaches. 
